## Requires ffmpeg and inotify-tools
# testing with this: inotifywait -m . -e create  --format '%w%f' | cat
# inotifywait --monitor . --event create --event move_to --event modify  --format '%w%f'

WATCH_FOLDER=/opt/watch


echo "Starting watch script ..."
ls -al /opt/watch

# Making sure the watch folder is mounted ...
if [[ ! -d $WATCH_FOLDER ]]; then
  echo "Watch folder not mounted!"
  exit 1
fi

# shellcheck disable=SC2095
# Setup a listener for the watch folder
inotifywait --monitor "${WATCH_FOLDER}" --event create --event move_to --format '%w%f' |
  while read -r fname; do
    echo "Triggered"
    # take off the mp4
    pathAndName=${fname%.mp4}

    # take off the path from the file name
    videoname=${pathAndName##*/}

    # take off the file name from the path
    videopath=$pathAndName:h

    mkdir -p ${videopath}/compressed/

    ffmpeg -y -r 30 -i ${fname} -vcodec libx265 -crf 18 -acodec mp3 -movflags +faststart ${videopath}/compressed/${videoname}-compressed.mp4

    echo "\033[1;33m compressed ${videoname}\n \033[0m"

  done

echo "Bye"
