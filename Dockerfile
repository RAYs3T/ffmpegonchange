FROM alpine
RUN apk --no-cache update && \
    apk --no-cache add \
    ffmpeg \
    inotify-tools

COPY encode.sh /opt/encode.sh
WORKDIR /opt

ENTRYPOINT /opt/encode.sh
